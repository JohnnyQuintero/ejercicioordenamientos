/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import Interface.IoperacionLista;
import java.util.Objects;

/**
 *
 * @author JHONY QUINTERO
 */
public class ListaS<T> implements IoperacionLista {

    private Nodo<T> cabeza = null;
    private int tam = 0;

    public ListaS() {
    }

    public void añadirInicio(T info) {
        this.cabeza = new Nodo(info, this.cabeza);
        this.tam++;
    }

    public void añadirFinal(T info) {
        if (this.esVacio()) {
            this.añadirInicio(info);
        } else {
            try {
                Nodo<T> x = this.getPos(tam - 1);
                x.setSig(new Nodo(info, null));
                this.tam++;
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public void insertarOrdenado(T info) {
        if (this.esVacio()) {
            this.añadirInicio(info);
        } else {
            Nodo<T> inicio = this.cabeza;
            Nodo<T> x = inicio;
            while (x != null) {
                Comparable cmp = (Comparable) info;
                int rta = cmp.compareTo(x.getInfo());
                if (rta < 0) {
                    break;
                }
                inicio = x;
                x = x.getSig();
            }
            if (inicio == x) {
                this.añadirInicio(info);
            } else {
                inicio.setSig(new Nodo(info, x));
                this.tam++;
            }
        }

    }

    private Nodo<T> getPos(int pos) {
        if (this.esVacio() || pos < 0 || pos >= this.getTam()) {
            throw new RuntimeException("Error en la posicion");
        }
        Nodo<T> x = this.cabeza;
        while (pos > 0) {
            x = x.getSig();
            pos--;
        }
        return x;
    }

    public T get(int pos) {
        Nodo<T> x = this.getPos(pos);
        return x.getInfo();
    }

    private Nodo<T> getMenordato(Nodo<T> actual) {
        for (Nodo<T> x = actual.getSig(); x != null; x = x.getSig()) {
            if (this.comparador(actual.getInfo(), x.getInfo()) > 0) {
                actual = x;
            }
        }
        return actual;
    }

    private Nodo<T>[] getMenor2(Nodo<T> actual) {
        Nodo<T> anterior = actual;
        for (Nodo<T> x = actual.getSig(); x != null; x = x.getSig()) {
            if (this.comparador(actual.getInfo(), x.getInfo()) > 0) {
                actual = x;
            } else {
                anterior = actual;
            }
        }
        Nodo menores[] = {anterior, actual};
        return menores;
    }

    private int comparador(T info1, T info2) {
        Comparable cmp = (Comparable) info1;
        return cmp.compareTo(info2);
    }

    public T getMenor() {
        if (this.esVacio()) {
            throw new RuntimeException("Lista vacia bobo");
        }
        Nodo<T> menor = this.getMenordato(cabeza);
        return menor.getInfo();
    }

    private void intercambiar(Nodo<T> menor, Nodo<T> actual) {
        T men = menor.getInfo();
        menor.setInfo(actual.getInfo());
        actual.setInfo(men);
    }

    public int getIndice(T info) {
        int i = 0;
        for (Nodo<T> x = this.cabeza; x != null; x = x.getSig()) {
            if (x.getInfo().equals(info)) {
                return (i);
            }
            i++;
        }
        return (-1);
    }

    private void intercambiar2(Nodo<T> menor, Nodo<T> actual) {
        Nodo<T> prevMenor = null, auxMenor = actual;
        while (auxMenor != null && auxMenor != menor) {
            prevMenor = auxMenor;
            auxMenor = auxMenor.getSig();
        }
        Nodo<T> prevActual = null, auxActual = this.cabeza;
        while (auxActual != null && auxActual != actual) {
            prevActual = auxActual;
            auxActual = auxActual.getSig();
        }
        if (prevActual != null) {
            prevActual.setSig(auxMenor);
            prevMenor.setSig(auxMenor.getSig());
            auxMenor.setSig(actual);
        } else {
            prevMenor.setSig(auxMenor.getSig());
            auxMenor.setSig(actual);
            this.cabeza = auxMenor;
        }
    }

    @Override
    public void ordenarSeleccion_Por_Infos() {
        if (this.esVacio()) {
            throw new RuntimeException("Lista vacia");
        }
        for (Nodo<T> actual = this.cabeza; actual != null; actual = actual.getSig()) {
            Nodo<T> menor = this.getMenordato(actual);
            if (this.comparador(menor.getInfo(), actual.getInfo()) < 0) {
                this.intercambiar(menor, actual);
            }
        }
    }

    @Override
    public void ordenarInserccion_Por_Infos() {
        if (this.esVacio()) {
            throw new RuntimeException("Lista vacia");
        }
        int i = 1;
        for (Nodo<T> x = this.cabeza.getSig(); x != null; x = x.getSig(), i++) {
            Nodo<T> aux = x;
            int j = i - 1;
            Nodo<T> y = this.cabeza;
            while (j >= 0 && y != null && this.comparador(y.getInfo(), aux.getInfo()) > 0) {
                this.intercambiar(y, aux);
                j = j - 1;
                y = y.getSig();
            }
        }
    }

    @Override
    public void ordenarInserccion_Por_Nodos() {
        if (this.esVacio()) {
            throw new RuntimeException("Lista vacia");
        }
        for (Nodo<T> menor = this.cabeza.getSig(); menor != null; menor = menor.getSig()) {
            for (Nodo<T> actual = this.cabeza; actual.getSig() != menor; actual = actual.getSig()) {
                if (comparador(menor.getInfo(), actual.getInfo()) < 0) {
                    intercambiar2(menor, actual);
                    break;
                }
            }
        }
    }

    public void set(int i, T info) {
        try {
            Nodo<T> x = this.getPos(i);
            x.setInfo(info);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private boolean esVacio() {
        return this.cabeza == null;
    }

    public int getTam() {
        return this.tam;
    }

    @Override
    public String toString() {
        String msg = "";
        for (Nodo<T> x = this.cabeza; x != null; x = x.getSig()) {
            msg += x.getInfo() + " ";
        }
        return msg + "null";
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.cabeza);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListaS<T> other = (ListaS<T>) obj;
        Nodo<T> a = this.cabeza;
        Nodo<T> b = other.cabeza;
        while (a != null && b != null) {
            T info1 = a.getInfo();
            T info2 = b.getInfo();
            if (!info1.equals(info2)) {
                return false;
            }
            a = a.getSig();
            b = b.getSig();
        }
        return a == null && b == null;
    }

}
