/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 *
 * @author JHONY QUINTERO
 */
class Nodo<T> {

    private T info;
    private Nodo<T> sig;

    Nodo() {

    }

    Nodo(T info, Nodo<T> sig) {
        this.info = info;
        this.sig = sig;
    }

    void setInfo(T info) {
        this.info = info;
    }

    T getInfo() {
        return this.info;
    }

    void setSig(Nodo<T> sig) {
        this.sig = sig;
    }

    Nodo<T> getSig() {
        return this.sig;
    }
}
