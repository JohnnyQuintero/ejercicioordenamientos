/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JHONY QUINTERO
 */
public class ListaSTest {

    public ListaSTest() {
    }

    @Test
    public void testAñadirFinal() {
        //Escenario 1
        ListaS<Integer> l = new ListaS();
        l.añadirFinal(1);
        l.añadirFinal(6);
        l.añadirFinal(13);
        l.añadirFinal(2);
        l.añadirFinal(3);
        //Resultado tam= 5 <1,6,13,2,3>
        boolean paso = false;
        if (l.getTam() == 5 && l.get(0) == 1 && l.get(1) == 6 && l.get(2) == 13
                && l.get(3) == 2 && l.get(4) == 3) {
            paso = true;
        }
        //Escenario 2
        ListaS<String> nom = new ListaS();
        nom.añadirFinal("Pepe");
        nom.añadirFinal("Franciso");
        nom.añadirFinal("Carro");
        nom.añadirFinal("Electroencefalografista");
        nom.añadirFinal("abaaabbabacc");
        nom.añadirFinal("Info");
        boolean paso2 = false;
        if (nom.getTam() == 6 && nom.get(0).equals("Pepe") && nom.get(1).equals("Franciso")
                && nom.get(2).equals("Carro") && nom.get(3).equals("Electroencefalografista")
                && nom.get(4).equals("abaaabbabacc") && nom.get(5).equals("Info")) {
            paso2 = true;
        }
        boolean rta = paso = paso2;
        assertTrue(rta);
    }

    @Test
    public void testInsertarOrdenado() {
        //Escenario 1
        ListaS<Integer> l = new ListaS();
        l.insertarOrdenado(6);
        l.insertarOrdenado(3);
        l.insertarOrdenado(99);
        l.insertarOrdenado(0);
        l.insertarOrdenado(78);
        // Esperado 
        boolean paso = false;
        if (l.getTam() == 5 && l.get(0) == 0 && l.get(1) == 3 && l.get(2) == 6
                && l.get(3) == 78 && l.get(4) == 99) {
            paso = true;
        }
        //Escenario 2
        ListaS<Integer> l2 = new ListaS();
        for (int i = 100; i >= 1; i--) {
            l2.insertarOrdenado(i);
        }

        ListaS<Integer> l_esperado = new ListaS();
        for (int i = 1; i <= 100; i++) {
            l_esperado.insertarOrdenado(i);
        }
        boolean paso2 = false;
        if (l2.getTam() == l_esperado.getTam() && l2.equals(l_esperado)) {
            paso2 = true;
        }

        assertTrue(paso);
        assertTrue(paso2);

    }

    @Test
    public void testSet() {
        //Escenario 1
        ListaS<Integer> l = new ListaS();
        l.añadirFinal(1);
        l.añadirFinal(1);
        l.añadirFinal(2);
        l.añadirFinal(5);
        l.añadirFinal(-10);
        //Actualizando la info
        l.set(4, 2);
        l.set(2, 10);
        /*
        Lista normal : <1,1,2,5,-10>
        Lista esperada : <1,1,10,5,2>
         */
        ListaS<Integer> l_espe = new ListaS();
        l_espe.añadirFinal(1);
        l_espe.añadirFinal(1);
        l_espe.añadirFinal(10);
        l_espe.añadirFinal(5);
        l_espe.añadirFinal(2);
        boolean paso = false;
        if (l.getTam() == l_espe.getTam() && l.equals(l_espe));
        paso = true;
        //Escenario 2
        ListaS<String> l2 = new ListaS();
        l2.añadirFinal("dfjhkjsdbfkjbsdjvhafjshdv");
        l2.añadirFinal("Pepe");
        l2.añadirFinal("Alejandro");
        l2.añadirFinal("info");
        l2.añadirFinal("Lista");
        //Actualizando
        l2.set(0, "Palabra");
        l2.set(1, "Nombre");
        l2.set(3, "Correcto");
        /*
        Lista esperada : <Palabra,Nombre,Alejandro, Correcto,Lista>
         */
        boolean paso2 = false;
        ListaS<String> l_espera = new ListaS();
        l_espera.añadirFinal("Palabra");
        l_espera.añadirFinal("Nombre");
        l_espera.añadirFinal("Alejandro");
        l_espera.añadirFinal("Correcto");
        l_espera.añadirFinal("Lista");
        if (l2.getTam() == l_espera.getTam() && l2.equals(l_espera)) {
            paso2 = true;
        }
        assertTrue(paso);
        assertTrue(paso2);
    }

}
